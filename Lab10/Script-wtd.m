RGB = imread('peppers.png');
m = size(RGB,1);
n = size(RGB,2);
YCBCR = rgb2ycbcr(RGB);
Y = YCBCR(:,:,1);
figure(1); image(RGB);
figure(2); imgage(Y);

%decomposição da imagem atraves do filtro de haar
[CA, CH, CV, CD] = dwt2(Y, 'haar');

%reconstrução da imgem
Ysyn = idwt2(CA, CH, CV, CD, 'haar');
RGBsyn = ycbcr2rgb(YCBCR);

figure(4);
subplot(2,2,1), imagesc(CA); title('CA');
subplot(2,2,2), imagesc(CH); title('CH');
subplot(2,2,3), imagesc(CV); title('CV');
subplot(2,2,4), imagesc(CD); title('CD');
colormap(gray);

figure(5);
subplot(2,2,1), imshow(RGB); title('RGB');
subplot(2,2,2), imagesc(Y, [0 255]); title('Y');
subplot(2,2,3), imagesc(RGBsyn); title('RGBsys');
subplot(2,2,4), imagesc(Ysyn); title('Ysys');

[m1, n1] = size(CA);
CZ = zeros(m1,n1);

Ysyn75 = idwt2(CA, CZ, CZ, CZ, 'haar');
YCBCR75syn = YCBCR;
YCBCR75syn(:,:,1) = Ysyn75;
RGB75syn = ycbcr2rgb(YCBCR75syn);
figure(6);
subplot(2,2,1), imshow(RGB); title('RGB');
subplot(2,2,2), imagesc(Y, [0 255]); title('Y');
subplot(2,2,3), imagesc(RGB75syn); title('RGB75sys');
subplot(2,2,4), imagesc(Ysyn75, [0 255]); title('Ysys75');

%segunda analise
figure(7);
[CA2, CH2, CV2, CD2] = dwt2(CA, 'haar');
subplot(2,2,1), imagesc(CA); title('CA');
subplot(2,2,2), imagesc(CA2); title('CA2');
[m2, n2] = size(CA2);
CZ2 = zeros(m2, n2);

%ficou faltando reconstruir a image








