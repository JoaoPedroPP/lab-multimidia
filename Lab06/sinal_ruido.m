hn=PQMF32_prototype;
figure(10);
plot(hn);
hn_db = 10*log10(abs(fft(hn)));
figure(20);
plot(hn_db);
PQMF32_Gfilters = zeros(32, 512);
for i = 0:31
 t2 = ((2*i+1)*pi/(2*32))*((0:511)+16);
 PQMF32_Gfilters(i+1,:) = hn.*cos(t2);
 PQMF32_Gfilters_db = 10*log10(abs(fft(PQMF32_Gfilters(1+i,:))));
 figure(30)
 hold on
 plot(PQMF32_Gfilters_db)
end




[input_signal,Fs] = wavread('..\..\Downloads\Matlab_MPEG\violin.wav');
input_frame=zeros(512,1);
output_signal=zeros(size(input_signal));
n_frames=(length(input_signal)-512+32)/32;
for i=1:n_frames

 % Overlap input_frames (vetores colunas)
 input_frame=input_signal((i-1)*32+1:(i-1)*32+512);
 % Filtragem e downsampling
 % Sinais em sub-band signals nas colunas
 subbands(i,:) = (PQMF32_Gfilters*input_frame)';
 % Quantiza��o Uniforme em 4 bits, usando um quantizador mid-thread [-1,+1]
 n_bits = 4;
 alpha = 2^(n_bits-1);
 quantized_subbands(i,:) = (floor(alpha*subbands(i,:)+0.5))/alpha;
% mid-thread
 % Filtro de S�ntese
 output_frame = PQMF32_Gfilters'*quantized_subbands(i,:)';
 % Overlap output_frames (com delay de 511 amostras)
 output_signal((i-1)*32+1:(i-1)*32+512)= output_signal((i-1)*32+1:(i-1)*32+512)+output_frame;
%figure()
%hold on
%specgram(output_signal,1024,fs,256);

end
figure(1);
specgram(output_signal,1024,Fs,256);
soundsc(output_signal,Fs);

error=output_signal-input_signal;
[signal_psd,w]=periodogram(input_signal(11001:12024), hamming(1024));
[error_psd,w]=periodogram(error(11001:12024), hamming(1024));
figure(2);
plot(w/pi*22050,10*log10(signal_psd));
hold on;
plot(w/pi*22050,10*log10(error_psd),'r','linewidth',2);
hold off;
set(gca,'xlim',[0 22050]);
legend('DEP do Sinal', 'Erro na DEP');
xlabel('Frequencia (Hz)');
ylabel('Magnitude (dB)');
snr_4bits=snr(input_signal(512:end-512),output_signal(512:end-512),0)

figure(3);
plot(quantized_subbands(100:200,2));
hold on;
plot(subbands(100:200,2),'--r');
hold off;
legend('Sub-banda n�2 Quantizada', 'Sub-banda n�2 Original');
xlabel('Tempo (amostras em Fs/32)'); ylabel('Amplitude');

n_frames=fix(n_frames/12)*12;
for k=1:12:n_frames

 % C�lculo de fator de escala em cada 12 amostras na sub-banda
 [scale_factors,tmp]=max(abs(subbands(k:k+11,:)));

 % Quantiza��o Adaptiva de 4 bits uniforme, mid-thread em [-Max,+Max]
 for j=1:32 % para cada sub-banda
 n_bits = 4;
 alpha = 2^(n_bits-1)/scale_factors(j);
 quantized_subbands(k:k+11,j) =(floor(alpha*subbands(k:k+11,j)+0.5))/alpha; % mid-thread
 end;
end;
figure(4);
plot(quantized_subbands(100:200,2));
hold on;
plot(subbands(100:200,2),'--r');
hold off;
legend('Sub-banda n�2 Quantizada', 'Sub-banda n�2 Original');
xlabel('Tempo (amostras em Fs/32)'); ylabel('Amplitude');

output_signal=zeros(size(input_signal));
for i=1:n_frames
 % Filtros de S�ntese
 output_frame = PQMF32_Gfilters'*quantized_subbands(i,:)';
 % Overlap output_frames (com delay de 511 amostras)
 output_signal((i-1)*32+1:(i-1)*32+512)= output_signal((i-1)*32+1:(i-1)*32+512)+output_frame;
 %specgram(output_signal,1024,fs,256);
end
figure(5);
specgram(output_signal,1024,Fs,256);
soundsc(output_signal,Fs);

error=output_signal-input_signal;
[signal_psd,w]=periodogram(input_signal(11001:12024), hamming(1024));
[error_psd,w]=periodogram(error(11001:12024),hamming(1024));
figure(6);
plot(w/pi*22050,10*log10(signal_psd));
hold on;
plot(w/pi*22050,10*log10(error_psd),'r','linewidth',2);
hold off;
set(gca,'xlim',[0 22050]);
legend('DEP do Sinal', 'DEP do Erro');
xlabel('Frequencia (Hz)');
ylabel('Magnitude (dB)');
snr_4bits_scaled=snr(input_signal(512:end-512),output_signal(512:end-512),0)

frame=input_signal(11001:11512);
[SMR, min_threshold,frame_psd_dBSPL]= MPEG1_psycho_acoustic_model1(frame);
% Pot�ncia m�xima para um sinal de [-1,+1] corresponde a 96 dB SPL
f = (0:255)/512*44100;
% Fun��o de Limiar Auditivo
auditory_threshold_dB = 3.64*((f/1000).^-0.8) - ...
 6.5*exp(-0.6.*((f/1000)-3.3).^2) + 0.001*((f/1000).^4);
figure(7);
plot(f, frame_psd_dBSPL, f, min_threshold,'.r', f, auditory_threshold_dB, '-.k');
hold off;
axis([0 22050 -20 100]);
legend('DEP do Sinal', 'Limiar M�nimo por sub-banda','Limiar Absoluto');
xlabel('Frequencia (Hz)'); ylabel('Magnitude (dB)');

% Alocando bits para uma taxa de bit de 192 kbits/s (compress�o = 4)
[N_bits, SNR] = MPEG1_bit_allocation(SMR, 192000);
figure(8);
stairs((0:32)/32*22050,[SMR SMR(32)]);
hold on;
stairs((0:32)/32*22050,[SNR SNR(32)],'--');
axis([0 22050 -20 100]);
legend('SMR', 'SNR');
xlabel('Frequencia (Hz)'); ylabel('Magnitude (dB)');

n_frames=fix(n_frames/12)*12;
for k=1:12:n_frames

 [scale_factors,tmp]=max(abs(subbands(k:k+11,:)));
 % Calculando SMRs
 frame=input_signal(176+(k-1)*32:176+(k-1)*32+511);
 % SMR = MPEG1_psycho_acoustic_model1(frame);
 % Alocando bits para uma taxa de bit de 192 kbits/s (compress�o = 4)
 N_bits = MPEG1_bit_allocation(SMR, 192000);
 % Quantiza��o Uniforme Perceptual Adaptiva, usando quantizador
% mid-thread em [-Max,+Max]
 for j=1:32 % para cada sub-banda
 if N_bits(j)~=0
 alpha = 2^(N_bits(j)-1)/scale_factors(j);
 quantized_subbands(k:k+11,j) = ...
 (floor(alpha*subbands(k:k+11,j)+0.5))/alpha; % mid-thread
 else
 quantized_subbands(k:k+11,j) = 0;
 end;
 end;
 % Sa�da no console
 fprintf('Frame Processado %3d/%3d\n', k,n_frames)
end;

output_signal=zeros(size(input_signal));
for i=1:n_frames
 % Filtros de S�ntese
 output_frame = PQMF32_Gfilters'*quantized_subbands(i,:)';
 % Overlap output_frames (com delay de 511 amostras)
 output_signal((i-1)*32+1:(i-1)*32+512)= output_signal((i-1)*32+1:(i-1)*32+512)+output_frame;
end
figure(9);
specgram(output_signal,1024,Fs,256);
soundsc(output_signal,Fs);

% sub-banda n�2.
figure(11);
plot(subbands(100:200,2),'--r');
hold on;
plot(quantized_subbands(100:200,2));
hold off;
legend('Sub-banda n�2 Original','Sub-banda n�2 Quantizada');
xlabel('Tempo (amostras em Fs/32)'); ylabel('Amplitude');
figure(10);
plot(subbands(100:200,20),'--r'); hold on;
plot(quantized_subbands(100:200,20)); hold off;
legend('Sub-banda n�20 Original','Sub-banda n�20 Quantizada');
xlabel('Tempo (amostras em Fs/32)'); ylabel('Amplitude');

error=output_signal(1:end)-input_signal(1:end);
[signal_psd,w]=periodogram(input_signal(11001:12024),hamming(1024));
[error_psd,w]=periodogram(error(11001:12024),hamming(1024));
figure(12);
plot(w/pi*22050,10*log10(signal_psd));
hold on;
plot(w/pi*22050,10*log10(error_psd),'r','linewidth',2);
hold off;
legend('DEP do Sinal', 'PED do Erro');
xlabel('Frequencia (Hz)'); ylabel('Magnitude (dB)');
snr_scaled_perceptual=snr(input_signal(512:end-512),...
 output_signal(512:end-512),0)

bits_per_block=sum(N_bits)*12+270
bit_rate=bits_per_block*44100/384