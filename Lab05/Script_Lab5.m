% Gerar sinal de varredura
Fs=8000;
input_signal=chirp((0:4*Fs)/Fs,0,4,4000);
sound(input_signal,Fs);
figure(1)
% visualizar espectrograma
specgram(input_signal,1024,Fs,256);

% Decimar-Interpolar por 2
downsampled=input_signal(1:2:end);
upsampled(1:2:2*length(downsampled))=2*downsampled;
sound(upsampled,Fs);
figure(2);
specgram(upsampled,1024,Fs,256);

% Filtro PB de s�ntese
G0=fir1(1000,0.5);
figure(3)
freqz(G0,1);
G0_output=filter(G0,1,upsampled);
figure(4)
specgram(G0_output(1001:end),1024,Fs);

% Filtro PB de an�lise
H0=G0;
H0_output=filter(H0,1,input_signal);
downsampled=H0_output(1:2:end);
upsampled(1:2:2*length(downsampled))=2*downsampled;
G0_output=filter(G0,1,upsampled);
Figure(5)
specgram(G0_output(2001:end),1024,Fs, 256);

% Filtros PA de s�ntese e an�lise
Gl=fir1(1000,1/2,'high');
Hl=Gl;
Hl_output=filter(Hl,1,input_signal);
downsampled=Hl_output(1:2:end);
upsampled(1:2:2*length(downsampled))=2*downsampled;
G1_output=filter(Gl,1,upsampled);
figure(6)
specgram(G1_output(2001:end),1024,Fs, 256);

% sinal sintetizado
synt_signal=G0_output+G1_output;
figure(7);
specgram(synt_signal(2001:end),1024,Fs, 256);

% erro
error=synt_signal(1001:end)-input_signal(1:end-1000);
figure(8)
plot(error)

a = 0;
i = 0;
for i = 0:31001
    a = a + (error[i])^2;
end
%printf(a);

% QMF PB
H0_QMF=[-0.006443977 0.02745539 -0.00758164 -0.0913825 0.09808522
0.4807962];
H0_QMF=[H0_QMF fliplr(H0_QMF)];
Figure(9)
hold on
freqz(H0_QMF,1);

% QMF PA
H1_QMF=H0_QMF.*[1 -1 1 -1 1 -1 1 -1 1 -1 1 -1];
hold on
freqz(H1_QMF,1);

% LF band
H0_output=filter(H0_QMF,1,input_signal);
subband_0=H0_output(1:2:end);
upsampled(1:2:2*length(subband_0))=2*subband_0;
G0_QMF = H0_QMF;
G0_output=filter(G0_QMF,1,upsampled);
figure(10)
specgram(G0_output(2001:end),1024,Fs, 256);

% HF band
H1_output=filter(H1_QMF,1,input_signal);
subband_1=H1_output(1:2:end);
upsampled(1:2:2*length(subband_0))=2*subband_1;
G1_QMF = -H1_QMF;
G1_output=filter(G1_QMF,1,upsampled);
figure(11)
specgram(G1_output(2001:end),1024,Fs, 256);

% S�ntese e Calculo do Erro
synt_signal=G0_output+G1_output;
figure(12)
specgram(synt_signal(2001:end),1024,Fs, 256);
error=synt_signal(1001:end)-input_signal(1:end-1000);
figure(13)
plot(error)

a = 0;
i = 0;
for i = 0:31001
    a = a + (error[i])^2;
end
%printf(a);

hn=PQMF32_prototype;

PQMF32_Gfilters = zeros(32, 512);
for i = 0:31
 t2 = ((2*i+1)*pi/(2*32))*((0:511)+16);
 PQMF32_Gfilters(i+1,:) = hn.*cos(t2);
end
PQMF32_Hfilters=fliplr(PQMF32_Gfilters);
figure(14)
for i = 1:32
 [H,W]=freqz(PQMF32_Hfilters(i,:),1,512,44100);
 plot(W,20*log10(abs(H))); hold on
end
xlabel('Frequencia (Hz)'); ylabel('Magnitude (dB)');
set(gca,'xlim',[0 22050]);
hold off;

[input_signal,Fs]=wavread('..\..\Downloads\audio\violin.wav');
figure(15);
specgram(input_signal,1024,Fs,256);
soundsc(input_signal,Fs);
output_signal=zeros(size(input_signal));

for i=1:32
    Hi_output=filter(PQMF32_Hfilters(i,:),1,input_signal);
    subband_i=Hi_output(1:32:end);
    upsampled_i(1:32:32*length(subband_i))=subband_i;
    % filtros de s�ntese s�o sim�tricos aos de an�lise, garantido fase linear
    % em cada banda
    Gi_output=filter(PQMF32_Gfilters(i,:),1,upsampled_i');
    output_signal=output_signal+Gi_output;
    % sa�da no console
    fprintf('processando a sub-banda %3d\n', i);
    % salvando a sub-banda 3
    if i==3
     G3_output=Gi_output;
    end;
end;

figure(16);
specgram(G3_output,1024,Fs,256);
soundsc(G3_output,Fs);

figure(17);
specgram(output_signal,1024,Fs,256);
soundsc(output_signal,Fs);

figure(18);
hold on;
error=output_signal(512:end)-input_signal(1:end-511);
[signal_psd,w]=periodogram(input_signal(11001:12024),hamming(1024));
[error_psd,w]=periodogram(error(11001:12024),hamming(1024));
plot(w/pi*22050,10*log10(signal_psd));
plot(w/pi*22050,10*log10(error_psd),'r','linewidth',2); hold off;
set(gca,'xlim',[0 22050]);
legend('DEP do Sinal', 'Erro da DEP');
xlabel('Frequencia (Hz)'); ylabel('Magnitude (dB)');
snr_PQMF=snr(input_signal(1:end-511),output_signal(512:end),0)