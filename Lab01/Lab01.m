clc
clear all
close all

%Vamos fazer um tom (um seno ou cosseno)
fa = 20000;
f1 = 250;
f2 = 9650;

t = (0:10000)/fa;

y1 = sin(2*pi*f1*t);    %son abafado baixa frequencia
y2 = sin(2*pi*f2*t);    %son agudo alta frequencia

y = y1 + y2;

sound(y1, fa);
sound(y2, fa);
sound(y, fa);   %PARTE 1 TERMINA AQUI;

dft_y1 = abs(fft(y1));  %Calcula o espectro de amplitude [0; 2pi]
dft_y2 = abs(fft(y2));  %Calcula o espectro de amplitude [0; 2pi]
dft_y = abs(fft(y));  %Calcula o espectro de amplitude [0; 2pi]
frad = 0:2/(length(t)-1):2; %Delimita x


figure();   %Abre uma figura
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), y2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), y(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), plot(frad, dft_y1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(324), plot(frad, dft_y2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(326), plot(frad, dft_y, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

%Amplitude -> dB

dft_y1_db = 20*log10(dft_y1);
dft_y2_db = 20*log10(dft_y2);
dft_y_db = 20*log10(dft_y); %PARTE 2 TERMINA AQUI

%Converte freq de rad para Hz
fHz = 0:fa/(length(t)-1):fa;

figure()
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), y2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), y(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), plot(fHz, dft_y1_db, 'LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fa/2 -30 100])
subplot(324), plot(fHz, dft_y2_db, 'LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fa/2 -30 100])
subplot(326), plot(fHz, dft_y_db, 'LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fa/2 -30 100])

%%Reamostragem do sinal (Decimacao)

tr = t(1:2:end);
yr1 = y1(1:2:end);
yr2 = y2(1:2:end);
yr = y(1:2:end);

%DFT do novo sinal

dft_yr1 = abs(fft(yr1));  %Calcula o espectro de amplitude [0; 2pi]
dft_yr2 = abs(fft(yr2));  %Calcula o espectro de amplitude [0; 2pi]
dft_yr = abs(fft(yr));  %Calcula o espectro de amplitude [0; 2pi]

dft_yr1_db = 20*log10(dft_yr1);
dft_yr2_db = 20*log10(dft_yr2);
dft_yr_db = 20*log10(dft_yr);

fr = 10000;

frHz = 0:fr/(length(tr)-1):fr;

figure();   %Abre uma figura
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), hold on,
stem(tr(1:50), yr1(1:50), 'rx', 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(323), stem(t(1:100), y1(1:100), 'LineWidth', 2), hold on,
stem(tr(1:50), yr2(1:50), 'rx', 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(325), stem(t(1:100), y1(1:100), 'LineWidth', 2), hold on,
stem(tr(1:50), yr2(1:50), 'rx', 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), plot(frHz, dft_yr1_db, 'r','LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fr/2 -30 100])
subplot(324), plot(frHz, dft_yr2_db, 'r','LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fr/2 -30 100])
subplot(326), plot(frHz, dft_yr_db, 'r','LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fr/2 -30 100])

sound(y1, fa)
sound(yr1, fr)

sound(y2, fa)
sound(yr2, fr)

sound(y, fa)
sound(yr, fr)   %PARTE 3 TERMINA AQUI

%%Filtragem passa baixa

H_pb = [1 1]; %Resposta em aplitude do filtro e a dft

% Calcular a resposta em frequencia do filtro
[h, w] = freqz(H_pb, 1, 'whole', 2001);
figure()
plot(w/pi, 20*log10(abs(h)), 'LineWidth', 2), xlabel('Freq [\pi rad/amostras]'), ylabel('Amplitude [dB]'), axis([0 2 -80 20]), grid on

z1 = filter(H_pb, 1, y1);
z2 = filter(H_pb, 1, y2);
z = filter(H_pb, 1, y);

sound(y1, fa)
sound(z1, fa)

sound(y2, fa)
sound(z2, fa)

sound(y, fa)
sound(z, fa)