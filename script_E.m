for i = 1:31
	t2 = ((2*i+1)*pi/(2*32))*((0:511)+16);
	PQMF32_Gfilters(i+1,:) = hn.*cos(t2);
	PQMF32_Gfilters_db = 10*log10(abs(fft(PQMF32_Gfilters(i+1,:))));
	hold on
	plot(PQMF32_Gfilters_db)
end