fa = 20000;
f1 = 250;
f2 = 9650;

t = (0:10000)/fa;

y1 = sin(2*pi*f1*t);    %son abafado baixa frequencia
y2 = sin(2*pi*f2*t);    %son agudo alta frequencia

y = y1 + y2;

dft_y1 = abs(fft(y1));
dft_y1_db = 20*log10(dft_y1);

frad = 0:2/(length(t)-1):2;
fHz = 0:fa/(length(t)-1):fa;

%sound(y1, fa);
%sound(y2, fa);
%sound(y, fa);

figure()
subplot(141), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel("Tempo"), ylabel("Amplitude")
subplot(142), plot(frad, dft_y1, 'LineWidth', 2), xlabel("n rad/s"), ylabel("Amplitude")
subplot(143), plot(frad, dft_y1_db, 'LineWidth', 2), xlabel("n rad/s"), ylabel("Amplitude em dB"), grid on
subplot(144), plot(fHz, dft_y1_db, 'LineWidth', 2), xlabel("Freq Hz"), ylabel("Amplitude em dB"), grid on
