clc
clear all
close all

%Vamos fazer um tom (um seno ou cosseno)
fa = 20000;
f1 = 250;
f2 = 9650;

t = (0:10000)/fa;

y1 = sin(2*pi*f1*t);    %son abafado baixa frequencia
y2 = sin(2*pi*f2*t);    %son agudo alta frequencia

y = y1 + y2;

sound(y1, fa);
sound(y2, fa);
sound(y, fa);

dft_y1 = abs(fft(y1));  %Calcula o espectro de amplitude [0; 2pi]. Isso resulta em dois pulsos, um a abs(f/fa)
dft_y2 = abs(fft(y2));  %Calcula o espectro de amplitude [0; 2pi]
dft_y = abs(fft(y));  %Calcula o espectro de amplitude [0; 2pi]
frad = 0:2/(length(t)-1):2; %Delimita x


figure();   %Abre uma figura
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), y2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), y(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), plot(frad, dft_y1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(324), plot(frad, dft_y2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(326), plot(frad, dft_y, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

%Amplitude -> dB

dft_y1_db = 20*log10(dft_y1);
dft_y2_db = 20*log10(dft_y2);
dft_y_db = 20*log10(dft_y); %PARTE 2 TERMINA AQUI

%Converte freq de rad para Hz
fHz = 0:fa/(length(t)-1):fa;

figure()
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), y2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), y(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), plot(fHz, dft_y1_db, 'LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fa/2 -30 100])
subplot(324), plot(fHz, dft_y2_db, 'LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fa/2 -30 100])
subplot(326), plot(fHz, dft_y_db, 'LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fa/2 -30 100])

%%Reamostragem do sinal (Decimacao)

tr = t(1:2:end);
yr1 = y1(1:2:end);
yr2 = y2(1:2:end);
yr = y(1:2:end);

%DFT do novo sinal

dft_yr1 = abs(fft(yr1));  %Calcula o espectro de amplitude [0; 2pi]
dft_yr2 = abs(fft(yr2));  %Calcula o espectro de amplitude [0; 2pi]
dft_yr = abs(fft(yr));  %Calcula o espectro de amplitude [0; 2pi]

dft_yr1_db = 20*log10(dft_yr1);
dft_yr2_db = 20*log10(dft_yr2);
dft_yr_db = 20*log10(dft_yr);

fr = 10000;

frHz = 0:fr/(length(tr)-1):fr;

figure();   %Abre uma figura
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), hold on,
stem(tr(1:50), yr1(1:50), 'rx', 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(323), stem(t(1:100), y1(1:100), 'LineWidth', 2), hold on,
stem(tr(1:50), yr2(1:50), 'rx', 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(325), stem(t(1:100), y1(1:100), 'LineWidth', 2), hold on,
stem(tr(1:50), yr2(1:50), 'rx', 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), plot(frHz, dft_yr1_db, 'r','LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fr/2 -30 100])
subplot(324), plot(frHz, dft_yr2_db, 'r','LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fr/2 -30 100])
subplot(326), plot(frHz, dft_yr_db, 'r','LineWidth', 2), xlabel('Freq [Hz]'), ylabel('Amplitude'), grid on, axis([0 fr/2 -30 100])

sound(y1, fa)
sound(yr1, fr)

sound(y2, fa)
sound(yr2, fr)

sound(y, fa)
sound(yr, fr)   %PARTE 3 TERMINA AQUI

%%Filtragem passa baixa

H_pb = [1 1]; %Resposta em aplitude do filtro e a dft

% Calcular a resposta em frequencia do filtro
[h, w] = freqz(H_pb, 1, 'whole', 2001);
figure()
plot(w/pi, 20*log10(abs(h)), 'LineWidth', 2), xlabel('Freq [\pi rad/amostras]'), ylabel('Amplitude [dB]'), axis([0 2 -80 20]), grid on

z1 = filter(H_pb, 1, y1);
z2 = filter(H_pb, 1, y2);
z = filter(H_pb, 1, y);

sound(y1, fa)
sound(z1, fa)

sound(y2, fa)
sound(z2, fa)

sound(y, fa)
sound(z, fa)

figure()
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), y2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), y(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), stem(t(1:100), z1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(324), stem(t(1:100), z2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(326), stem(t(1:100), z(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

%fft dos sinais filtrados

dft_z1 = abs(fft(z1));
dft_z2 = abs(fft(z2));
dft_z = abs(fft(z));

figure()
subplot(321), plot(frad, dft_y1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(323), plot(frad, dft_y2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(325), plot(frad, dft_y, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

subplot(322), plot(frad, dft_z1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(324), plot(frad, dft_z2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(326), plot(frad, dft_z, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

%%Filtragem passa alta

H_pa = [1 -1]; %Resposta em aplitude do filtro e a dft
% Calcular a resposta em frequencia do filtro passa alta
[ha, wa] = freqz(H_pa, 1, 'whole', 2001);
figure()
plot(wa/pi, 20*log10(abs(ha)), 'LineWidth', 2), xlabel('Freq [\pi rad/amostras]'), ylabel('Amplitude [dB]'), axis([0 2 -80 20]), grid on

za1 = filter(H_pa, 1, y1);
za2 = filter(H_pa, 1, y2);
za = filter(H_pa, 1, y);

sound(y1, fa)
sound(za1, fa)

sound(y2, fa)
sound(za2, fa)

sound(y, fa)
sound(za, fa)

figure()
subplot(321), stem(t(1:100), y1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), y2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), y(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

subplot(322), stem(t(1:100), za1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(324), stem(t(1:100), za2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(326), stem(t(1:100), za(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

%fft dos sinais filtrados

dft_za1 = abs(fft(za1));
dft_za2 = abs(fft(za2));
dft_za = abs(fft(za));

figure()
subplot(321), plot(frad, dft_y1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(323), plot(frad, dft_y2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(325), plot(frad, dft_y, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

subplot(322), plot(frad, dft_za1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(324), plot(frad, dft_za2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(326), plot(frad, dft_za, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

%%Reamostragem do sinal apos o filtro passa baixa

tpb = t(1:1:end);
zrpb1 = z1(1:1:end);
zrpb2 = z2(1:1:end);
zrpb = z(1:1:end);

dft_zrpb1 = abs(fft(zrpb1));
dft_zrpb2 = abs(fft(zrpb2));
dft_zrpb = abs(fft(zrpb));

figure()
subplot(331), plot(frad, dft_y1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(334), plot(frad, dft_y2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(337), plot(frad, dft_y, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

subplot(332), plot(frad, dft_za1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(335), plot(frad, dft_za2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(338), plot(frad, dft_z, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

subplot(333), plot(frad, dft_zrpb1, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(336), plot(frad, dft_zrpb2, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on
subplot(339), plot(frad, dft_zrpb, 'LineWidth', 2), xlabel('\pi rad/amostras'), ylabel('Amplitude'), grid on

%%Interpolacao e reconstrucao
ti = t(1:1:end);
zi1 = zrpb1(1:1:end);
zi2 = zrpb2(1:1:end);
zi = zrpb(1:1:end);

%interp_zi1 = interp(zi1, 2*length(zi1));
%interp_zi2 = interp(zi2, 2*length(zi2));
%interp_zi = interp(zi, 2*length(zi));

figure();   %Abre uma figura
subplot(321), stem(t(1:100), zi1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(323), stem(t(1:100), zi2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
subplot(325), stem(t(1:100), zi(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on

%subplot(322), stem(t(1:100), interp_zi1(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
%subplot(324), stem(t(1:100), interp_zi2(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
%subplot(326), stem(t(1:100), interp_zi(1:100), 'LineWidth', 2), xlabel('tempo'), ylabel('Amplitude'), grid on
